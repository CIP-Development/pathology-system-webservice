<?php

require 'init.php';

$requestId = $_POST["requestId"];
$essayId = $_POST["essayId"];

$sql = "SELECT id, rows, columns, totalSlots, usedSlots, availableSlots, ctrlQty FROM Support WHERE requestId = {$requestId} AND essayId = {$essayId};";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('id'=>$row['id'], 'rows'=>$row['rows'], 'columns'=>$row['columns'], 'totalSlots'=>$row['totalSlots'], 'usedSlots'=>$row['usedSlots'], 'availableSlots'=>$row['availableSlots'], 'ctrlQty'=>$row['ctrlQty']));
}

echo json_encode($response);

mysqli_close($connection);

?>