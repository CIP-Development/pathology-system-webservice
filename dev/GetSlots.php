<?php

require 'init.php';

$requestId = $_POST["requestId"];
$essayId = $_POST["essayId"];

$sql = "SELECT ReadingData.agentId AS agentId, 
            ReadingData.sampleId AS sampleId, 
            ReadingData.supportId AS supportId, 
            ReadingData.requestId AS requestId, 
            ReadingData.essayId AS essayId, 
            ReadingData.cellPosition AS cellPosition, 
            ReadingData.cResult AS result, 
            ReadingData.readingDataTypeId AS typeId, 
            ReadingData.cropId AS cropId, 
            ReadingData.workFlowId AS workFlowId, 
            ReadingData.activityId AS activityId, 
            ReadingData.numOrderId AS numOrderId,
            Sample.numOrder AS labProcessId
        FROM ReadingData
            INNER JOIN Sample
                ON Sample.id = ReadingData.sampleId
        WHERE ReadingData.requestId = {$requestId}
            AND ReadingData.essayId = {$essayId}
            AND ReadingData.result = 'qualitative';";

$result = mysqli_query($connection, $sql);

$response = array();

$pointer = 0;

while($row = mysqli_fetch_array($result)){
    $pointer = $pointer + 1;
    array_push($response, array('labProcessId'=>$row['labProcessId'], 
                                'agentId'=>$row['agentId'], 
                                'sampleId'=>$row['sampleId'], 
                                'supportId'=>$row['supportId'], 
                                'requestId'=>$row['requestId'], 
                                'essayId'=>$row['essayId'], 
                                'cellPosition'=>$row['cellPosition'], 
                                'result'=>$row['result'], 
                                'type'=>$row['typeId'], 
                                'cropId'=>$row['cropId'], 
                                'workFlowId'=>$row['workFlowId'], 
                                'activityId'=>$row['activityId'], 
                                'numOrderId'=>$row['numOrderId']
                            )
                        );
}

echo json_encode($response);

mysqli_close($connection);

?>