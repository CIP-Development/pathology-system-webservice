<?php

require 'init.php';

$requestId = $_POST["requestId"];
$essayId = $_POST["essayId"];
$agentId = $_POST["agentId"];

$sql = "SELECT sampleId, supportId, cellPosition, result
 FROM ReadingData WHERE requestId = {$requestId} AND essayId = {$essayId} AND agentId = {$agentId};";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, array('sampleId'=>$row['sampleId'], 'supportId'=>$row['supportId'], 'cellPosition'=>$row['cellPosition'], 'result'=>$row['result']));
};

echo json_encode($response);
mysqli_close($connection);

?>