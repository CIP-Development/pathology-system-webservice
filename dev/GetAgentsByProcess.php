<?php

require 'init.php';

$requestId = $_POST["requestId"];

$sql = "SELECT AgentByRequestProcessDetail.agentId as agentId, Agent.shortName as agentName, AgentByRequestProcessDetail.requestId as requestId, AgentByRequestProcessDetail.essayId as essayId, Agent.agentGroupId as agentGroupId
    FROM AgentByRequestProcessDetail 
    INNER JOIN Agent ON Agent.id = AgentByRequestProcessDetail.agentId WHERE requestId = {$requestId};";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('agentId'=>$row['agentId'], 'agentName'=>$row['agentName'], 'requestId'=>$row['requestId'], 'essayId'=>$row['essayId'], 'agentGroupId'=>$row['agentGroupId']));
}

echo json_encode($response);

mysqli_close($connection);

?>