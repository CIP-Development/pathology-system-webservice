<?php

require 'init.php';

$sql = "SELECT Request.id as requestId, code, Crop.longName as cropName, details, RequestProcess.agentQty as pathogenQty, RequestProcess.sampleQty as sampleQty, Request.registeredBy as requiredDate, RequestProcess.numOrderId as numOrderId 
    FROM Request 
    INNER JOIN RequestProcess ON Request.id = RequestProcess.requestId 
    INNER JOIN Crop ON RequestProcess.cropId = Crop.id 
    WHERE Request.sampleQty IS NOT NULL AND requestStatusId != 65
    ORDER BY Request.id DESC;";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, 
    array(
        'requestId'=>$row['requestId'], 
        'code'=>$row['code'], 
        'cropName'=>$row['cropName'], 
        'details'=>$row['details'], 
        'pathogenQty'=>$row['pathogenQty'], 
        'sampleQty'=>$row['sampleQty'], 
        'requiredDate'=>$row['requiredDate'],
        'numOrderId' => $row['numOrderId']
    ));
}

echo json_encode($response);
mysqli_close($connection);

?>