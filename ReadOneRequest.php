<?php

require 'init.php';

$id = $_GET["id"];
$cipCode = $_GET["cipCode"];
$sampleQty = $_GET["sampleQty"];
$pathogenQty = $_GET["pathogenQty"];
$details = $_GET["details"];
$diagnostic = $_GET["diagnostic"];
$cropId = $_GET["cropId"];
$workFlowId = 1;
$userId = 3;
$paymentTypeId = 24;
$requestStatusId = 4;
$requiredDate = $_GET["requiredDate"];


$sql = "SELECT cipCode, Crop.longName as cropName, details, pathogenQty, sampleQty, requiredDate FROM Request INNER JOIN Crop ON Request.cropId = Crop.id WHERE Request.id = ${id}";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('cipCode'=>$row['cipCode'], 'cropName'=>$row['cropName'], 'details'=>$row['details'], 'pathogenQty'=>$row['pathogenQty'], 'sampleQty'=>$row['sampleQty'], 'requiredDate'=>$row['requiredDate']));
}

echo json_encode($response);
mysqli_close($connection);

?>