<?php

require 'init.php';

$requestId = $_POST["requestId"];
$essayId = $_POST["essayId"];

$sql = "SELECT  s.id, 
                s.rows, 
                s.columns, 
                s.totalSlots, 
                s.usedSlots, 
                s.availableSlots, 
                s.ctrlQty, 
                s.workFlowId, 
                s.cropId, 
                s.numOrderId, 
                s.supportTypeId, 
                s.requestId, 
                s.essayId,
                s.activityId,
                s.agentDetail
        FROM Support s
        WHERE requestId = {$requestId} 
            AND essayId = {$essayId};";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('supportId'=>$row['id'], 
                                'columns'=>$row['columns'], 
                                'rows'=>$row['rows'],                             
                                'totalSlots'=>$row['totalSlots'], 
                                'usedSlots'=>$row['usedSlots'], 
                                'availableSlots'=>$row['availableSlots'], 
                                'ctrlQty'=>$row['ctrlQty'], 
                                'workFlowId'=>$row['workFlowId'], 
                                'cropId'=>$row['cropId'], 
                                'numOrderId'=>$row['numOrderId'], 
                                'activityId'=>$row['activityId'], 
                                'supportTypeId'=>$row['supportTypeId'], 
                                'requestId'=>$row['requestId'], 
                                'essayId'=>$row['essayId'], 
                                'agentName'=>$row['agentDetail']
                        ));
}

echo json_encode($response);

mysqli_close($connection);

?>