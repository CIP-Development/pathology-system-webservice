<?php

require 'init.php';

$requestId = $_POST["requestId"];
$numOrderId = $_POST["numOrderId"];

$sql = "SELECT RequestProcessDetail.requestId as requestId, Crop.longName as cropName, RequestProcessDetail.cropId as cropId, RequestProcessDetail.workFlowId as workFlowId, RequestProcessDetail.essayId as essayId, Essay.shortName as essayName, RequestProcess.sampleQty as sampleQty, Location.shortName as locationName, RequestProcessDetail.activityId as activityId, Activity.shortName as activityName, RequestProcessDetail.numOrderId as numOrderId 
FROM RequestProcessDetail 
INNER JOIN Essay ON RequestProcessDetail.essayId = Essay.id 
INNER JOIN Location ON Essay.locationId = Location.id 
INNER JOIN Crop ON RequestProcessDetail.cropId = Crop.id 
INNER JOIN RequestProcess ON RequestProcess.requestId = RequestProcessDetail.requestId AND RequestProcess.numOrderId = RequestProcessDetail.numOrderId
INNER JOIN Activity ON Activity.id = RequestProcessDetail.activityId
WHERE RequestProcessDetail.requestId = {$requestId} AND RequestProcessDetail.numOrderId = {$numOrderId} GROUP BY RequestProcessDetail.essayId;";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, array('requestId'=>$row['requestId'], 'cropName'=>$row['cropName'], 'cropId'=>$row['cropId'], 'workFlowId'=>$row['workFlowId'], 'essayId'=>$row['essayId'],'essayName'=>$row['essayName'],'sampleQty'=>$row['sampleQty'],'locationName'=>$row['locationName'], 'activityId'=>$row['activityId'], 'activityName'=>$row['activityName'], 'numOrderId'=>$row['numOrderId']));
};

echo json_encode($response);
mysqli_close($connection);

?>