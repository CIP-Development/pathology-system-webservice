<?php

require 'init.php';

$sql = "SELECT ReadingData.requestId AS requestId, Request.code AS requestCode, COUNT(ReadingData.id) AS rduQty, Essay.shortName as essayName, Agent.shortName as agentName, ReadingData.essayId AS essayId, ReadingData.agentId AS agentId
	FROM ReadingData
		INNER JOIN Request ON Request.id = ReadingData.requestId
		INNER JOIN Essay ON Essay.id = ReadingData.essayId
        INNER JOIN Agent ON Agent.id = ReadingData.agentId
	GROUP BY ReadingData.requestId, ReadingData.essayId, ReadingData.agentId;";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('requestId'=>$row['requestId'], 'requestCode'=>$row['requestCode'], 'rduQty'=>$row['rduQty'], 'essayName'=>$row['essayName'], 'agentName'=>$row['agentName'], 'essayId'=>$row['essayId'], 'agentId'=>$row['agentId']));
};

echo json_encode($response);
mysqli_close($connection);

?>