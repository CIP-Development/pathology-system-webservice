<?php

require 'init.php';

$requestId = $_POST["requestId"];

$sql = "SELECT ReadingData.cResult as result, ReadingData.sampleId as sampleId FROM ReadingData WHERE ReadingData.requestId = {$requestId};";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, array('result'=>$row['result'], 'sampleId'=>$row['sampleId']));
};

echo json_encode($response);
mysqli_close($connection);

?>