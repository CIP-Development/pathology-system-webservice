<?php
header('Access-Control-Allow-Origin: *');
require 'init.php';

$sql = "
    SELECT Request.id as requestId, 
        Request.code as code, 
        Crop.longName as cropName, 
        Request.details as details, 
        Request.creationDate as creationDate,
        RequestProcess.agentQty as pathogenQty, 
        RequestProcess.sampleQty as sampleQty, 
        Request.registeredBy as requiredDate, 
        RequestProcess.numOrderId as numOrderId,
        RequestProcess.essayDetail as essayDetail,
        Parameter.shortName as sampleType
    FROM Request 
    INNER JOIN RequestProcess ON Request.id = RequestProcess.requestId 
    INNER JOIN Crop ON RequestProcess.cropId = Crop.id
    INNER JOIN Sample ON Request.id = Sample.requestId 
    INNER JOIN Parameter ON Sample.sampleTypeId = Parameter.id
    WHERE Request.sampleQty IS NOT NULL AND requestStatusId != 65
    GROUP BY Request.id, RequestProcess.numOrderId
    ORDER BY Request.id DESC, RequestProcess.numOrderId ASC;
    ";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, 
    array(
        'requestId'=>$row['requestId'], 
        'code'=>$row['code'], 
        'cropName'=>$row['cropName'], 
        'details'=>$row['details'],
        'creationDate' => $row['creationDate'],
        'pathogenQty'=>$row['pathogenQty'], 
        'sampleQty'=>$row['sampleQty'], 
        'requiredDate'=>$row['requiredDate'],
        'numOrderId' => $row['numOrderId'],
        'essayDetail' => $row['essayDetail'],
        'sampleType' => $row['sampleType']
    ));
}

echo json_encode($response);
mysqli_close($connection);

?>