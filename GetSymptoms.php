<?php

require 'init.php';

$sql = "SELECT s.id as id, s.shortName as shortName, s.longName as longName FROM Symptom s WHERE s.status = 'active' AND s.id !=1;";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('symptomId'=>$row['id'], 'shortName'=>$row['shortName'], 'longName'=>$row['longName']));
}

echo json_encode($response);

mysqli_close($connection);

?>