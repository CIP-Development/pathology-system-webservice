<?php

require 'init.php';

$agentId = $_POST["agentId"];
$sampleId = $_POST["sampleId"];
$cellPosition = $_POST["cellPosition"];
$result = $_POST["result"];
$supportId = $_POST["supportId"];
$readingDataTypeId = $_POST["readingDataTypeId"];
$evidenceId = $_POST["evidenceId"];
$essayId = $_POST["essayId"];
$requestId = $_POST["requestId"];
$cropId = $_POST["cropId"];
$workflowId = $_POST["workflowId"];

$sql = "INSERT INTO ReadingData (agentId, sampleId, cellPosition, result, supportId, readingDataTypeId, essayId, requestId, cropId, workflowId, status)
VALUES ({$agentId}, {$sampleId}, {$cellPosition}, '{$result}', {$supportId}, {$readingDataTypeId}, {$essayId}, {$requestId}, {$cropId}, {$workflowId}, 'active');";

$result = mysqli_query($connection, $sql);

if($result)
		{
			$response=array(
				'status' => 1,
				'status_message' =>'DataUnit Added Successfully.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'DataUnit Addition Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);


mysqli_close($connection);

?>