<?php

require 'init1.php';

$requestId = $_POST["requestId"];

$sql = "SELECT Request.code AS requestCode, 
    ReadingData.supportId AS supportId, 
    Request.registeredAt AS registeredAt,
    ReadingData.cResult AS result, 
    ReadingData.sampleId AS sampleId, 
    Agent.shortName AS agentName,
    GROUP_CONCAT(DISTINCT Agent.shortName SEPARATOR ', ') AS agentName
FROM ReadingData 
INNER JOIN Request ON Request.id = ReadingData.requestId
INNER JOIN Agent ON Agent.id = ReadingData.agentId
INNER JOIN Symptom ON Symptom.id = ReadingData.symptomId
WHERE ReadingData.requestId = {$requestId}
GROUP BY ReadingData.sampleId;";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, array(
        'serol1_request_invivo'=>$row['requestCode'], 
        'serol1memb'=>$row['supportId'], 
        'serol1sent_invivo'=>$row['registeredAt'], 
        'serol1result_invivo'=>$row['result'], 
        'sampleId'=>$row['sampleId'], 
        'virusaftertest1'=>$row['agentName']
    ));
};

echo json_encode($response);
mysqli_close($connection);

?>