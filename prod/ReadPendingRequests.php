<?php

require 'init.php';

$sql = "SELECT id, code, registeredBy, registeredAt, requiredDate, sampleQty, agentQty, requestStatusId as statusId, approvalDate
 FROM Request
 WHERE requestStatusId = 4
 ORDER BY Request.id DESC;";

$result = mysqli_query($connection, $sql);
$response = array();
while($row = mysqli_fetch_array($result)){
    array_push($response, array('id'=>$row['id'], 'code'=>$row['code'], 'registeredBy'=>$row['registeredBy'], 'registeredAt'=>$row['registeredAt'], 'requiredDate'=>$row['requiredDate'], 'sampleQty'=>$row['sampleQty'], 'agentQty'=>$row['agentQty'], 'statusId'=>$row['statusId'], 'approvalDate'=>$row['approvalDate']));
}

echo json_encode($response);
mysqli_close($connection);

?>