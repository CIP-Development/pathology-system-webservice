﻿<?php

require 'init.php';

$data = json_decode(file_get_contents('php://input'), true);
$response = array();

foreach($data as $support){

	$supportId = $support["supportId"];
	$columns = $support["columns"];
	$rows = $support["rows"];
	$totalSlots = $support["totalSlots"];
	$usedSlots = $support["usedSlots"];
	$availableSlots = $support["availableSlots"];
	$ctrlQty = $support["ctrlQty"];
	$supportTypeId = $support["supportTypeId"];
	$requestId = $support["requestId"];
	$essayId = $support["essayId"];
	$activityId = $support["activityId"];
	$cropId = $support["cropId"];
	$workFlowId = $support["workFlowId"];
	$numOrderId = $support["numOrderId"];

	$sql = "INSERT INTO Support (id, columns, rows, totalSlots, usedSlots, availableSlots, ctrlQty, supportTypeId, requestId, essayId, activityId, cropId, workFlowId, numOrderId, status)
	VALUES ({$supportId}, {$columns}, {$rows}, {$totalSlots}, {$usedSlots}, {$availableSlots}, {$ctrlQty}, {$supportTypeId}, {$requestId}, {$essayId}, {$activityId}, {$cropId}, {$workFlowId}, {$numOrderId},'active');";

	$result = mysqli_query($connection, $sql);

	if($result){
		array_push($response, array(
			'status' => 1,
			'status_message' =>'Support Added Successfully.'
		));
	}else{
		array_push($response, array(
			'status' => 0,
			'status_message' =>'Support Addition Failed.'
		));
	};
	
	header('Content-Type: application/json');		
};

echo json_encode($response);

mysqli_close($connection);

?>