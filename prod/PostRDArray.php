<?php

require 'init.php';

$data = json_decode(file_get_contents('php://input'), true);
$response = array();

foreach($data as $rdu){

	$agentId = $rdu["agentId"];
	$sampleId = $rdu["sampleId"];
	$cellPosition = $rdu["cellPosition"];
	$result = $rdu["result"];
	$supportId = $rdu["supportId"];
	$activityId = $rdu["activityId"];
	$essayId = $rdu["essayId"];
	$requestId = $rdu["requestId"];
	$cropId = $rdu["cropId"];
	$workFlowId = $rdu["workFlowId"];
	$numOrderId = $rdu["numOrderId"];
	$symptomId = $rdu["symptomId"];
	$readingDataTypeId = $rdu["readingDataTypeId"];

	$registeredBy;
	$recordType;
	$rduStatus;

	if($essayId == 1) {
		$registeredBy = 'm.ramos';
	} else if ($essayId == 2){
		$registeredBy = 'd.cotera';
	} else if ($essayId == 3 || $essayId == 5){
		$registeredBy = 'j.arellano';
	} else {
		$registeredBy = 'y.gomez';
	};

	if($essayId == 2 || $essayId == 3) {
		$recordType = 'qualitatives';
	} else {
		$recordType = 'qualitative';
	};

	if($result == 'empty'){
		$rduStatus = 'disabled';
	} else {
		$rduStatus = 'active';
	}

	$sql = "INSERT INTO ReadingData (agentId, sampleId, cellPosition, cResult, supportId, activityId, essayId, requestId, cropId, workFlowId, numOrderId, symptomId, readingDataTypeId, status, result, registeredBy)
	VALUES ({$agentId}, {$sampleId}, {$cellPosition}, '{$result}', {$supportId}, {$activityId}, {$essayId}, {$requestId}, {$cropId}, {$workFlowId}, {$numOrderId}, ${symptomId}, ${readingDataTypeId}, '{$rduStatus}', '{$recordType}', '{$registeredBy}');";

	$result = mysqli_query($connection, $sql);

	if($result){
		array_push($response, array(
			'status' => 1,
			'status_message' =>'ReadingDataUnit '.$requestId.' '.$cellPosition.' '.$registeredBy.' Added Successfully.'
		));
	}else{
		array_push($response, array(
			'status' => 0,
			'status_message' =>'ReadingDataUnit '.$requestId.' '.$cellPosition.' '.$registeredBy.' Addition Failed.'
		));
	};
	
};

header('Content-Type: application/json');
echo json_encode($response);

mysqli_close($connection);

?>