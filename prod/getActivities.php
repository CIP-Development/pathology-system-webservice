<?php

require 'init.php';

$sql = "SELECT ActivityByEssay.activityId as activityId, Activity.shortName as activityName, ActivityByEssay.essayId as essayId 
FROM ActivityByEssay 
INNER JOIN Activity ON Activity.id = ActivityByEssay.activityId";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('activityId'=>$row['activityId'], 'activityName'=>$row['activityName'], 'essayId'=>$row['essayId']));
}

echo json_encode($response);
mysqli_close($connection);

?>