<?php

require 'init.php';

$columns = $_POST["columns"];
$rows = $_POST["rows"];
$totalSlots = $_POST["totalSlots"];
$usedSlots = $_POST["usedSlots"];
$availableSlots = $_POST["availableSlots"];
$ctrlQty = $_POST["ctrlQty"];
$supportTypeId = $_POST["supportTypeId"];
$requestId = $_POST["requestId"];

$sql = "INSERT INTO Support (columns, rows, totalSlots, usedSlots, availableSlots, ctrlQty, supportTypeId, requestId, status)
VALUES ({$columns}, {$rows}, {$totalSlots}, {$usedSlots}, {$availableSlots}, {$ctrlQty}, {$supportTypeId}, {$requestId}, 'active');";

$result = mysqli_query($connection, $sql);

if($result){
	$response=array(
		'status' => 1,
		'status_message' =>'Support Added Successfully.'
	);
}else{
	$response=array(
		'status' => 0,
		'status_message' =>'Support Addition Failed.'
	);
		}
		header('Content-Type: application/json');
		echo json_encode($response);


mysqli_close($connection);

?>