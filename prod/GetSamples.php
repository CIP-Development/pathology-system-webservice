<?php

require 'init.php';

$requestId = $_POST["requestId"];
$numOrderId = $_POST["numOrderId"];

$sql = "SELECT id as sampleId, sampleTypeId, requestId, numOrderId, numOrder 
        FROM Sample 
        WHERE requestId = {$requestId} 
            AND numOrderId = {$numOrderId} 
            AND sampleTypeId <> 1 ;";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('sampleId'=>$row['sampleId'], 
                                'accessionCode'=>$row['accessionCode'], 
                                'collectingCode'=>$row['collectingCode'], 
                                'sampleTypeId'=>$row['sampleTypeId'], 
                                'requestId'=>$row['requestId'], 
                                'numOrderId'=>$row['numOrderId'],
                                'numOrder'=>$row['numOrder'],
                        ));
}

echo json_encode($response);

mysqli_close($connection);

?>