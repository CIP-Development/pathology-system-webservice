<?php

require 'init.php';

$data = json_decode(file_get_contents('php://input'), true);
$response = array();

foreach($data as $rdu){

	$agentId = $rdu["agentId"];
	$sampleId = $rdu["sampleId"];
	$cellPosition = $rdu["cellPosition"];
	$result = $rdu["result"];
	$supportId = $rdu["supportId"];
	$activityId = $rdu["activityId"];
	$essayId = $rdu["essayId"];
	$requestId = $rdu["requestId"];
	$cropId = $rdu["cropId"];
	$workFlowId = $rdu["workFlowId"];
	$numOrderId = $rdu["numOrderId"];
	$symptomId = $rdu["symptomId"];
	$readingDataTypeId = $rdu["readingDataTypeId"];

	$sql = "UPDATE ReadingData SET cResult = '{$result}'
	WHERE agentId = {$agentId} 
    AND sampleId = {$sampleId} 
    AND cellPosition = {$cellPosition} 
    AND supportId = {$supportId} 
    AND activityId = {$activityId} 
    AND essayId = {$essayId} 
    AND requestId = {$requestId} 
    AND workFlowId = {$workFlowId} 
    AND numOrderId = {$numOrderId} 
    AND readingDataTypeId = {$readingDataTypeId};";

	$result = mysqli_query($connection, $sql);

	if($result){
		array_push($response, array(
			'status' => 1,
			'status_message' =>'ReadingDataUnit '.$requestId.' '.$cellPosition.' Updated Successfully.'
		));
	}else{
		array_push($response, array(
			'status' => 0,
			'status_message' =>'ReadingDataUnit '.$requestId.' '.$cellPosition.' Updating Failed.'
		));
	};
	
};

header('Content-Type: application/json');
echo json_encode($response);

mysqli_close($connection);

?>