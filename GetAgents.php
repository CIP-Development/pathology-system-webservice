<?php

require 'init.php';

$requestId = $_POST["requestId"];

$sql = "SELECT RequestProcessDetail.agentId as agentId, Agent.shortName as agentName FROM RequestProcessDetail INNER JOIN Agent ON Agent.id = RequestProcessDetail.agentId WHERE requestId = {$requestId};";

$result = mysqli_query($connection, $sql);

$response = array();

while($row = mysqli_fetch_array($result)){
    array_push($response, array('agentId'=>$row['agentId'], 'agentName'=>$row['agentName']));
}

echo json_encode($response);

mysqli_close($connection);

?>