<?php

require 'init.php';

$data = json_decode(file_get_contents('php://input'), true);
$response = array();

foreach($data as $request){

	$id = $request["id"];
	$statusId = $request["statusId"];

	$sql = "UPDATE Request SET Request.requestStatusId = {$statusId} WHERE Request.id = {$id};";

	$result = mysqli_query($connection, $sql);

	if($result){
		array_push($response, array(
			'status' => 1,
			'status_message' =>'Request Updated Successfully.'
		));
	}else{
		array_push($response, array(
			'status' => 0,
			'status_message' =>'Request Update Failed.'
		));
	};
	
	header('Content-Type: application/json');		
};

echo json_encode($response);

mysqli_close($connection);

?>